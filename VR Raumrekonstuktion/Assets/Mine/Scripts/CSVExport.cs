﻿using UnityEngine;
using System.IO;

public class CSVExport : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ExportCSV(Vector3[] data, string fileName)
	{
		fileName = fileName + ".csv";
		if (File.Exists(fileName))
		{
			Debug.Log(fileName + " already exists.");
			return;
		}

		
		Debug.Log("started converting");

		var sr = File.CreateText(fileName);

		for (int i = 0; i < data.Length; i++)
		{

			sr.WriteLine(data[i].x + "," + data[i].y + "," + data[i].z);
			
		}
		Debug.Log("done");
		
		
		
		sr.Close();
	}
}
