﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrackerDeviceSetter : MonoBehaviour {

    SteamVR_TrackedObject trackedObj;

	// Use this for initialization
	void Start () {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public void SetTrackingDevice(Dropdown dropDown)
    {
        trackedObj.SetDeviceIndex(dropDown.value);

    }
}
