﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KinectOffsetAdjuster : MonoBehaviour {

    Vector3 offset;

    // Use this for initialization
    void Start () {
        
    }
    
    // Update is called once per frame
    void Update () {
        
    }

    public void AdjustOffset(Slider slider)
    {
        switch (slider.name)
        {
            case "X":
                offset.x = slider.value;
                break;
            case "Y":
                offset.y = slider.value;
                break;
            case "Z":
                offset.z = slider.value;
                break;
            default:
                Debug.Log("Unknown Slider");
                break;
        }
        transform.localPosition = offset;
    }
}
