﻿using UnityEngine.UI;
using UnityEngine;

public class SliderValueDisplay : MonoBehaviour {

    Text theText;

    void Start()
    {
        theText = GetComponent<Text>();
    }

    public void SetValue(float value)
    {
        theText.text = value.ToString("#.00");
    }
        

        
}
